import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddHolderComponent } from './add-holder/add-holder.component';
import { ListHolderComponent } from './list-holder/list-holder.component';
import { HolderComponent } from './holder.component';
import { HolderRoutingModule } from './holder-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AddHolderComponent, ListHolderComponent, HolderComponent],
  imports: [
    CommonModule,
    HolderRoutingModule,
    ReactiveFormsModule
  ]
})
export class HolderModule { }
