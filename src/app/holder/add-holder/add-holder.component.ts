import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HolderService } from 'src/app/core/services/holder.service';
import { Holder } from 'src/app/core/model/Holder.model';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-add-holder',
  templateUrl: './add-holder.component.html',
  styleUrls: ['./add-holder.component.css']
})
export class AddHolderComponent implements OnInit {

  public formGroup: FormGroup;
  public loading = false;
  public error: string;
  @Input()
  public editHolder: Holder;

  @Output()
  public reload = new EventEmitter<boolean>();

  constructor(private holderService: HolderService) { 
    this.formGroup = new FormGroup({
      rut: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      identification: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      phone: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      foundationDate: new FormControl('', Validators.required),
      typePerson: new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
    this.fillForm();
    console.log(this.editHolder);
  }

  public onSubmit(){
    if (this.editHolder != null) {
      this.update();
    } else {
      this.save();
    }
  }

  public save() {
    const newHolder = {
      rut: this.formGroup.get('rut').value,
      name: this.formGroup.get('name').value,
      lastName: this.formGroup.get('lastName').value,
      identification:this.formGroup.get('identification').value,
      address: this.formGroup.get('address').value,
      phone: this.formGroup.get('phone').value,
      email: this.formGroup.get('email').value,
      foundationDate: this.formGroup.get('foundationDate').value,
      typePerson: this.formGroup.get('typePerson').value
    } as Holder;
    this.holderService.post(newHolder).pipe(
      finalize (
        () => {
          return this.loading = false;
        }
      )
    ).subscribe(
      response => {
        this.reload.next(true);
        this.formGroup.reset();
      },
      error => {
        this.error = error.error;
      }
    );
  }

  public update() {
    const newHolder = {
      rut: this.formGroup.get('rut').value,
      name: this.formGroup.get('name').value,
      lastName: this.formGroup.get('lastName').value,
      identification:this.formGroup.get('identification').value,
      address: this.formGroup.get('address').value,
      phone: this.formGroup.get('phone').value,
      email: this.formGroup.get('email').value,
      foundationDate: this.formGroup.get('foundationDate').value,
      typePerson: this.formGroup.get('typePerson').value
    } as Holder;
    this.holderService.update(this.editHolder.id + '', newHolder).pipe(
      finalize (
        () => {
          return this.loading = false;
        }
      )
    ).subscribe(
      response => {
        this.reload.next(true);
        this.formGroup.reset();
      },
      error => {
        this.error = error.error;
      }
    );
  }

  public fillForm() {
    if (this.editHolder != null) {
      this.formGroup = new FormGroup({
        rut: new FormControl(this.editHolder.rut, Validators.required),
        name: new FormControl(this.editHolder.name, Validators.required),
        lastName: new FormControl(this.editHolder.lastName, Validators.required),
        identification: new FormControl(this.editHolder.identification, Validators.required),
        address: new FormControl(this.editHolder.address, Validators.required),
        phone: new FormControl(this.editHolder.phone, Validators.required),
        email: new FormControl(this.editHolder.email, Validators.required),
        foundationDate: new FormControl(this.editHolder.foundationDate, Validators.required),
        typePerson: new FormControl(this.editHolder.typePerson, Validators.required)
      });
    }
  }

}
