import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';
import { Holder } from '../core/model/Holder.model';

@Component({
  selector: 'app-holder',
  templateUrl: './holder.component.html',
  styleUrls: ['./holder.component.css']
})
export class HolderComponent implements OnInit {

  public addHolder = false;
  public eventsSubject: Subject<boolean> = new Subject<boolean>();
  public editHolder: Holder;

  constructor() { }

  ngOnInit() {
  }

  public showAddHolder() {
    this.addHolder = !this.addHolder;
  }

  public onReloadEvent(reload: boolean) {
    if (reload) {
      this.eventsSubject.next(true);
      this.editHolder = null;
      this.addHolder = false;
    }
  }

  public onEditHolderEvent(holder: Holder) {
    this.editHolder = holder;
    console.log(holder);
    this.addHolder = true;
  }

}
