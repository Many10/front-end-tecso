import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HolderComponent } from './holder.component';

const holderRoutes: Routes = [
    {
        path: 'index',
        component: HolderComponent
    }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(holderRoutes)],
    exports: [RouterModule]
  })

  export class HolderRoutingModule { }