import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Holder } from 'src/app/core/model/Holder.model';
import { HolderService } from 'src/app/core/services/holder.service';
import { finalize } from 'rxjs/operators';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-list-holder',
  templateUrl: './list-holder.component.html',
  styleUrls: ['./list-holder.component.css']
})
export class ListHolderComponent implements OnInit {

  public holders: Array<Holder> = [];
  public error: string;
  public loading = false;
  private eventsSubscription: Subscription;
  @Input() 
  events: Observable<boolean>;
  @Output()
  holderEdit = new EventEmitter<Holder>();

  constructor(private holderService: HolderService) { }

  ngOnInit() {
    this.getHolderList();
    this.eventsSubscription = this.events.subscribe(() => this.getHolderList());
  }

  private getHolderList() {
    this.loading = true;
    this.holderService.getAll().pipe(
      finalize(
        () => this.loading = false
      )
    ).subscribe(
      (holders: Array<Holder>) => {
        if (holders != null) {
          this.holders = holders;
        }
      },
      error => {
        this.holders = [];
        this.error = error.error;
      }
    );
  }

  public editHolder(holder: Holder) {
    this.holderEdit.next(holder);
  }

  public deleteHolder(idHolder: number) {
    this.loading = true;
    this.holderService.delete(idHolder + '').pipe(
      finalize(
        () => this.loading = false
      )
    ).subscribe(
      response => {
          this.getHolderList();
      },
      error => {
        this.holders = [];
        this.error = error.error;
      }
    );
  }

}
