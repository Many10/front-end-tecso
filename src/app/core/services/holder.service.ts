import { Injectable } from '@angular/core';
import { ServiceService } from './service.service';
import { Holder } from '../model/Holder.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HolderService extends ServiceService<Holder> {

  constructor(http: HttpClient) {
    super();
    this.httpClient = http;
    this.resource = 'holder/';
   }
}
