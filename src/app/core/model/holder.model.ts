export class Holder {
    id: number;
    rut: string;
    name: string;
    lastName: string;
    identification: string;
    foundationDate: Date;
    phone: string;
    address: string;
    email: string;
    typePerson: string;
}