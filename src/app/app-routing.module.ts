import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './shared/dashboard/dashboard.component';

const holderModule = () => import('./holder/holder.module').then(mod => mod.HolderModule);

const appRoutes: Routes = [
  {
      path: 'dashboard',
      component: DashboardComponent,
      data: { title: 'Inicio' },
      children: [
        {
          path: 'holder',
          loadChildren: holderModule
        }
      ]
  },
  {
    path: '',
    redirectTo: '/dashboard/holder/index',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/dashboard/holder/index',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
